// $Id: DiscreteDensityEstimator.java 2473 2009-03-05 21:14:07Z nguyenda $

package gov.nih.ncgc.math.stat;

import java.util.Random;

public class DiscreteDensityEstimator extends DensityEstimator {
    
    protected double eps = 0.;

    public DiscreteDensityEstimator (int nbins) {
	super (nbins);
    }

    public DiscreteDensityEstimator (int nbins, int min, int max) {
	super (nbins, (double)min, (double)max);
    }

    public void estimate () {
	double totalWeight = 0.; // ignore -inf & +inf bins?
	for (int i = 0; i < bin.length; ++i) {
	    totalWeight += bin[i];
	}
	totalWeight += 2; // laplace-like estimator

	eps = 1./(bin.length+2);
	for (int i = 0; i < bin.length; ++i) {
	    bin[i] = (bin[i]+eps)/totalWeight;
	}
    }

    public double probability (double x) {
	int b = find (x);
	if (b >= 0 && b < bin.length) {
	    return bin[b];
	}
	return eps; // constant probability mass
    }

    public static void main (String[] argv) throws Exception {
	int N = 100;
	if (argv.length > 0) {
	    N = Integer.parseInt(argv[0]);
	}
	System.err.println("# bins = " + N);

	Random rand = new Random ();
	int M = rand.nextInt(500);
	System.err.println("## generating " + M + " random values from "
			   +"(pseudo) gaussian distribution");

	DensityEstimator pdf = new DiscreteDensityEstimator (N, -10, 10);

	double[] x = new double[M];
	for (int i = 0; i < M; ++i) {
	    x[i] = rand.nextGaussian();
	    pdf.increment(x[i]);
	}
	pdf.estimate();

	for (int i = 0; i < M; ++i) {
	    System.out.println(x[i] + " " + pdf.probability(x[i]));
	}
    }
}
