// $Id: GroupContrib.java 2461 2009-03-01 23:12:12Z nguyenda $

package gov.nih.ncgc.descriptor;

import java.io.IOException;
import java.io.InputStream;

import chemaxon.struc.Molecule;

public class GroupContrib extends AtomTyper {
    public GroupContrib () {
    }
    public GroupContrib (InputStream is) throws IOException {
	super (is);
    }
    public GroupContrib (String file) throws IOException {
	super (file);
    }

    public double getValue (Molecule mol) {
	String[] groups = apply (mol);
	double value = 0.;
	for (int i = 0; i < groups.length; ++i) {
	    String s = groups[i];
	    //System.out.println((i+1) + ": " + s);
	    if (s != null) {
		try {
		    value += Double.parseDouble(s);
		}
		catch (NumberFormatException ex) {
		    ex.printStackTrace();
		}
	    }
	}
	return value;
    }

    public static void main (String[] argv) throws Exception {
	if (argv.length < 2) {
	    System.out.println("usage: GroupContrib RULES FILES...");
	    System.exit(1);
	}
	
	GroupContrib gc = new GroupContrib (argv[0]);
	for (int i = 1; i < argv.length; ++i) {
	    chemaxon.formats.MolImporter mi = 
		new chemaxon.formats.MolImporter (argv[i]);
	    for (Molecule mol = new Molecule (); mi.read(mol); ) {
		System.out.println(mol.getName() + " " + gc.getValue(mol));
	    }
	    mi.close();
	}
    }
}
