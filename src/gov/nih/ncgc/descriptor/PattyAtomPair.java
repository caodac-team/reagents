// $Id: PattyAtomPair.java 3500 2009-10-29 16:00:48Z nguyenda $

package gov.nih.ncgc.descriptor;
import java.util.TreeMap;
import java.util.Map;

import chemaxon.struc.Molecule;
import chemaxon.formats.MolImporter;
import static gov.nih.ncgc.descriptor.PattyTyper.Type;

public class PattyAtomPair extends AtomPair {

    public PattyAtomPair (Molecule mol) {
	this (mol, -1);
    }

    public PattyAtomPair (Molecule mol, int maxPath) {
	Type[] types = PattyTyper.getInstance().getTypes(mol);
	int[][] D = allShortestPaths (mol);
	for (int i = 0; i < types.length; ++i) {
	    int a = types[i].ordinal();
	    for (int j = i+1; j < types.length; ++j) {
		int b = types[j].ordinal();
		int len = D[i][j];
		if (maxPath < 0 || len <= maxPath) {
		    String path = Type.getInstance(Math.max(a, b)) 
			+ "_" + len + "_" 
			+ Type.getInstance(Math.min(a, b));
		    Integer count = sparseVector.get(path);
		    sparseVector.put(path, count != null ? count+1 : 1);
		}
	    }
	}
    }

    public static void main (String argv[]) throws Exception {
	System.setProperty("ap.driver", PattyAtomPair.class.getName());
	AtomPair.main(argv);
    }
}
