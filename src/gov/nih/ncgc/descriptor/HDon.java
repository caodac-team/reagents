package gov.nih.ncgc.descriptor;

import java.io.InputStream;
import java.io.IOException;

public class HDon extends GroupContrib {
    private static HDon instance = null;
    
    private HDon (InputStream is) throws IOException {
	super (is);
    }

    public synchronized static GroupContrib getInstance () {
	if (instance == null) {
	    try {
		instance = new HDon
		    (HDon.class.getResourceAsStream("resources/hdon.txt"));
	    }
	    catch (IOException ex) {
		ex.printStackTrace();
	    }
	}
	return instance;
    }
}
