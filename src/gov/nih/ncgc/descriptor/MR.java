// $Id: MR.java 2461 2009-03-01 23:12:12Z nguyenda $

package gov.nih.ncgc.descriptor;

import java.io.InputStream;
import java.io.IOException;

public class MR extends GroupContrib {
    private static MR instance = null;
    
    private MR (InputStream is) throws IOException {
	super (is);
    }

    public synchronized static GroupContrib getInstance () {
	if (instance == null) {
	    try {
		instance = new MR 
		    (MR.class.getResourceAsStream("resources/MR.rules"));
	    }
	    catch (IOException ex) {
		ex.printStackTrace();
	    }
	}
	return instance;
    }
}
