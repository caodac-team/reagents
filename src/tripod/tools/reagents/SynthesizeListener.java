package tripod.tools.reagents;

public interface SynthesizeListener {
    void synthesized (SynthesizeEvent e);
}
