package tripod.tools.reagents;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;
import org.jdesktop.swingx.JXErrorPane;


public class StepWelcome extends StepCommon implements ActionListener {
    private static final Logger logger = Logger.getLogger
	(StepWelcome.class.getName());

    public static final String NAME = "Welcome";
    public static final String DESC = 
	"Welcome to NCGC's library synthesizer wizard. To begin, please"
	+" select whether to load from a previously saved project or "
	+"starting a new project. If you're not sure how to get started, "
	+"click on the \"Demo\" button.";

    File file;

    public StepWelcome () {
	super (NAME, DESC);
    }

    public void actionPerformed (ActionEvent e) {
	String cmd = e.getActionCommand();
	if ("load".equalsIgnoreCase(cmd)) {
	    JFileChooser chooser = getFileChooser ();
	    chooser.setDialogTitle("Please specify saved project");
	    int op = chooser.showOpenDialog(this);
	    if (JFileChooser.APPROVE_OPTION == op) {
		try {
		    file = chooser.getSelectedFile();
		    model.load(file);
		    model.setLastVisible(true);
		    //model.lastStep();
		}
		catch (Exception ex) {
		    JXErrorPane.showDialog(ex);
		}
	    }
	}
	else if ("demo".equalsIgnoreCase(cmd)) {
	    try {
		/*
		model.load(new File (StepWelcome.class.getResource
				     ("resources/demo.zip").toURI()));
		*/
		model.load(StepWelcome.class.getResourceAsStream
			   ("resources/demo.zip"));

		model.setLastVisible(true);
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Can't load demo", ex);
		JXErrorPane.showDialog(ex);
	    }
	}
	else {
	    model.clear();
	}

	model.nextStep();
    }

    @Override
    protected void initUI () {
	JPanel pane = new JPanel (new GridLayout (1, 2, 5, 0));
	JButton btn;
	pane.add(btn = new JButton ("Demo", new ImageIcon 
				    (StepWelcome.class.getResource
				     ("resources/arrow-circle.png"))));
	btn.setToolTipText("Load a demo project");
	btn.addActionListener(this);

	pane.add(btn = new JButton ("Load", new ImageIcon 
				    (StepWelcome.class.getResource
				     ("resources/arrow-270.png"))));
	btn.setToolTipText("Load from a previously save project");
	btn.addActionListener(this);
	pane.add(btn = new JButton ("New", new ImageIcon 
				    (StepWelcome.class.getResource
				     ("resources/arrow.png"))));
	btn.setToolTipText("Start a new project");
	btn.addActionListener(this);

	JPanel center = new JPanel ();
	center.add(pane);

	add (center);
    }

    @Override
    public void prepare () {
    }

    @Override
    public void applyState () throws InvalidStateException {
    }    
}
